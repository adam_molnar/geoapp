import React, { Component } from 'react';
import { Col, Row } from 'reactstrap';
import {Link} from 'react-router-dom';

class Header extends Component {
  render() {
    return (
      <div>
        <Row>
            <Col xs="6" sm="4">
              <ul className="nav navbar-nav">
                <li>
                  <Link to="/" >
                      <span className="glyphicon glyphicon-home" aria-hidden="true">
                        {this.props.linkToHome}
                      </span>
                    </Link>
                  </li>
                <li>
                  <Link to="/displaycountries">
                    <span className="glyphicon glyphicon-list-alt" aria-hidden="true">
                      {this.props.linkToDisplayCountries}
                    </span>
                  </Link>
                </li>
                <li>
                  <Link to="/about">
                    <span className="glyphicon glyphicon-user" aria-hidden="true">
                      {this.props.linkToAbout}
                    </span>
                  </Link>
                </li>
              </ul>
            </Col>
            <Col xs="6" sm="4">
                <h1>{this.props.title}</h1>
                <h2>{this.props.description}</h2>
            </Col>
        </Row>
      </div>
    );
  }
}

export default Header;