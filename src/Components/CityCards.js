import React, {Component} from 'react';
import { Card, CardImg, CardTitle, CardBlock } from 'reactstrap';

class CityCards extends Component {
    render() {    
        return(
            <div>
            <Card>
                <CardImg top width="100%" src={this.props.city.img} alt={this.props.city.name} />
                <CardBlock>
                <CardTitle>{this.props.city.name}</CardTitle>
                </CardBlock>
            </Card>
            </div>
        );
    }
}

export default CityCards;