import React, { Component } from 'react';
import { Jumbotron, Container } from 'reactstrap';
import Header from './Header';

class AboutTheProject extends Component {
  render() {
      let title = 'About The Project.';
      let linkToHome = "Home.";
      let linkToDisplayCountries = "DisplayCountries."
      let linkToAbout = "About"
    return (
      <div className="background">
        <Header 
            title={title}
            linkToHome = {linkToHome}
            linkToDisplayCountries = {linkToDisplayCountries}
            linkToAbout = {linkToAbout} 
        />
        <div>
            <Container fluid>
                <Jumbotron>
                    <h1 className="display-3">Summary</h1>
                    <p className="lead">This is a simple ReactJS trial-work application </p>
                    <hr className="my-2" />
                    <p> - Created with create-react-app</p>
                    <p> - SPA - react-router-dom is used</p>
                    <p> - Has 3 views: Home, DisplayCountries and About </p>
                    <p> - Has more than 3 components </p>
                    <p> - Components are communicating with each other via props and state </p>
                    <p> - Contains parent-child and child-parent (e.g. AddCountries and DisplayCountries) communications </p>
                    <p> - Contains external ReactJS component (react-data-grid) </p>
                    <p> - New datas can be added to the react-data-grid </p> 
                    <p> - Uses Sass instead of css </p>
                </Jumbotron>
            </Container>
        </div>
      </div>
    );
  }
}

export default AboutTheProject;