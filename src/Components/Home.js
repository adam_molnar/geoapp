import React, { Component } from 'react';
import CityCards from './CityCards';
import { Row, Col, Container } from 'reactstrap';
import londonImg from '../Images/london1.jpg';
import moscowImg from '../Images/moscow.jpg';
import parisImg from '../Images/paris.jpg';
import budapestImg from '../Images/budapest.jpg'
import washingtonImg from '../Images/washington.jpg'
import berlinImg from '../Images/berlin.jpg'
import '../App.css';


class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cityImages: [
                {   
                    'name' : 'london',
                    'img' : londonImg
                },
                {   
                    'name' : 'moscow',
                    'img' : moscowImg
                },
                {   
                    'name' : 'paris',
                    'img' : parisImg
                },
                {
                    'name' : 'budapest',
                    'img' : budapestImg
                },
                {
                    'name' : 'washington',
                    'img' : washingtonImg
                },
                {
                    'name' : 'berlin',
                    'img' : berlinImg
                }
            ]
        };
    }

    render() {
        let cityCards = this.state.cityImages.map((city, i) => {
            return (
                <Col key={i} xs="6" sm="4">
                    <CityCards city={city} />
                </Col>
            )
        })
        return (
            <Container>
                <Row>
                    {cityCards}
                </Row>
            </Container>
        );
    }
}

export default Home;