import React, { Component } from 'react';
import { Col, Row,  Container } from 'reactstrap';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

class AddCountries extends Component {
    constructor(props) {
        super(props);
        this.state = {
            countryName : '',
            countryCapital: '',
            countryCurrency: '',
            countryLanguage: '',
            countryRegion: '',
            newCountry : {}
        }
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    onAddCountries() {
        if (this.state.countryName === '' || this.state.countryCapital === '' || this.state.countryCurrency === '' || this.state.countryLanguage === '' || this.state.countryRegion === '') {
            alert('Fill all the cells!');
        }else {
            this.props.onAddCountry(this.state.newCountry);
        }
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        
        this.setState({
            [name]: value
        });
        this.setState({
            newCountry : {
                id : this.props.countries.length + 1,
                country : this.state.countryName,
                capital: this.state.countryCapital,
                currency: this.state.countryCurrency,
                language: this.state.countryLanguage,
                region: this.state.countryRegion
            }
        })
    }

    render() {
        return (
            <div className="background">
            <Container>
                    <Row>
                    <Form>
                        <FormGroup>
                            <Label for="newCountryName">Country</Label>
                            <Input 
                                name="countryName" 
                                type="text" 
                                value={this.state.countryName}
                                onChange={this.handleInputChange} 
                                placeholder="Country" 
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="newCountryCapital">Capital</Label>
                            <Input 
                                name="countryCapital" 
                                type="text" 
                                value={this.state.countryCapital} 
                                onChange={this.handleInputChange} 
                                placeholder="Capital" 
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="newCountryCapital">Currency</Label>
                            <Input 
                                name="countryCurrency" 
                                type="text" 
                                value={this.state.countryCurrency} 
                                onChange={this.handleInputChange} 
                                placeholder="Currency" 
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="newCountryLanguage">Language</Label>
                            <Input 
                                name="countryLanguage" 
                                type="text" 
                                value={this.state.countryLanguage} 
                                onChange={this.handleInputChange} 
                                placeholder="Language" 
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="newCountryRegion">Region</Label>
                            <Input 
                                name="countryRegion" 
                                type="text" 
                                value={this.state.countryRegion} 
                                onChange={this.handleInputChange} 
                                placeholder="Region" 
                            />
                        </FormGroup>
                        <Col  xs="6" sm="4">
                            <Button className="btn btn-primary"onClick={this.onAddCountries.bind(this)}>Add New Country</Button>
                        </Col>
                    </Form>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default AddCountries;