import React, {Component} from 'react';
import ReactDataGrid from 'react-data-grid';
import Header from './Header';
import AddCountries from './AddCountries'
import '../App.css';

class DisplayCountries extends Component {
    constructor(props) {
        super(props);
        this.state = {
            columns : [
                { key: 'id', name: 'ID' }, 
                { key: 'country', name: 'Country' }, 
                {key: 'capital', name: 'Capital'},
                {key: 'currency', name: 'Currency'}, 
                {key: 'language', name : 'Language'}
            ],
            countries : [
                { id: '1', country: 'England', capital: 'London', currency: 'Pound sterling', language: 'English', region :'EU' },
                { id: '2', country: 'Russia', capital: 'Moscow', currency: 'Russian ruble', language: 'Russian', region: 'EU'},
                { id: '3', country: 'Denmark', capital: 'Copenhagen', currency: 'Danish krone', language: 'Danish', region: 'EU'},
                { id: '4', country: 'Hungary', capital: 'Budapest', currency: 'Forint', language: 'Hungarian', region: 'EU'},
                { id: '5', country: 'Serbia', capital: 'Belgrade', currency: 'Serbian dinar', language: 'Serbian', region: 'EU'},
                { id: '6', country: 'Canada', capital: 'Ottawa', currency: 'Canadian dollar', language: 'English, French', region: 'NA'},
                { id: '7', country: 'USA', capital: 'Washington', currency: 'Dollar', language: 'English', region: 'NA'}
            ]
        }
    }

    onAddCountry(newCountry) {
        this.setState(prevState => ({
            countries: [...prevState.countries, newCountry]
        }))
    }

    rowGetter = rowNumber => this.state.countries[rowNumber];

    render() {
        let title = "DisplayCountries.";
        let description = "Go somewhere you’ve never been before.";
        let linkToHome = "Home.";
        let linkToDisplayCountries = "DisplayCountries."
        let linkToAbout = "About"
        return(
            <div className="background"> 
                <Header 
                    title={title} 
                    description={description} 
                    linkToHome={linkToHome} 
                    linkToDisplayCountries={linkToDisplayCountries}
                    linkToAbout={linkToAbout}    
                />
                <p>This table is created with react-data-grid</p>
                <ReactDataGrid
                    columns={this.state.columns}
                    rowGetter={this.rowGetter}
                    rowsCount={this.state.countries.length}
                    minHeight={300} />
                <AddCountries 
                    countries={this.state.countries}
                    onAddCountry={this.onAddCountry.bind(this)}
                />
            </div>
        );
    }
}

export default DisplayCountries;