import React, { Component } from 'react';
import './App.css';
import Home from './Components/Home'
import Header from './Components/Header'

class App extends Component {
  render() {
    let titleText = "GeoApp."
    let linkToDisplayCountries = "DisplayCountries."
    let home = "Home."
    let about = "About"
    return (
      <div className="background">
        <Header 
          title={titleText} 
          linkToHome={home} 
          linkToDisplayCountries={linkToDisplayCountries}
          linkToAbout={about}
        />
        <Home />
      </div>
    );
  }
}

export default App;